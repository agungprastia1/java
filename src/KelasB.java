public class KelasB extends KelasA{
    private String atributB;

    public String getAtributB() {
        return atributB;
    }

    public void setAtributB(String atributB) {
        this.atributB = atributB;
    }

    public static void main(String[] args) {
        KelasB kelasB = new KelasB();
        kelasB.getAtributA();
    }
}
