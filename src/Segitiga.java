public class Segitiga {
   private int panjang ,tinggi ,lebar ;

    Segitiga(int panjang, int tinggi, int lebar){
        this.panjang = panjang;
        this.tinggi = tinggi;
        this.lebar = lebar;
    }

    public int volume(int p ,int t, int l){
        return p*l*t;
    }

    public int getPanjang() {
        return panjang;
    }

    public void setPanjang(int panjang) {
        this.panjang = panjang;
    }

    public int getTinggi() {
        return tinggi;
    }

    public void setTinggi(int tinggi) {
        this.tinggi = tinggi;
    }

    public int getLebar() {
        return lebar;
    }

    public void setLebar(int lebar) {
        this.lebar = lebar;
    }
}

